/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: Constant.java
 * Date: 2020-03-22
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.client.common;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/21
 */
public class Constant {
    /**
     * License行数,至少3行
     */
    public static final int LICENSE_LINES = 3;
    /**
     * 时间格式
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:ss:mm";
    /**
     * 时间分隔符
     */
    public static final String DATE_SPLIT = "&&";
}
